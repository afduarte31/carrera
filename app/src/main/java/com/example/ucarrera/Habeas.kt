package com.example.ucarrera

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Habeas : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_habeas)

        val btnBACK:Button = findViewById(R.id.btnBack)
        btnBACK.setOnClickListener { onBackPressed() }
    }
}