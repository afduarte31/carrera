package com.example.ucarrera

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView

class Resultados : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultados)
        setSupportActionBar(findViewById(R.id.toolbarPP))

        //Donde los datos estan almacenados
        val datos = application as DatosEst

        //Donde se imprimen los datos almacenados en la clase DatosEst.kt
        val tvShowNombre: TextView = findViewById(R.id.TVShowNombreR)
        val tvShowCodigo: TextView = findViewById(R.id.TVShowCodigoR)
        val tvShowCorreo: TextView = findViewById(R.id.TVShowCorreoR)

        tvShowNombre.text = "Nombres: ${datos.Nombre}"
        tvShowCodigo.text = "Codigo: ${datos.Codigo}"
        tvShowCorreo.text = "Correo: ${datos.Correo}"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.Desarrolador->{
            startActivity(Intent(this, Desarrollador::class.java))
            true
        }
        R.id.Inicio->{
            startActivity(Intent(this, MainActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}