package com.example.ucarrera

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class MateriaAdapter(private val materiaList: List<Materia>): RecyclerView.Adapter<MateriaViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MateriaViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MateriaViewHolder(layoutInflater.inflate(R.layout.item_materia, parent, false))
    }

    override fun onBindViewHolder(holder: MateriaViewHolder, position: Int) {
        val item = materiaList[position]
        holder.show(item)
    }

    override fun getItemCount(): Int {
        return materiaList.size
    }

}