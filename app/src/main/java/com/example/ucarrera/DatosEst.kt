package com.example.ucarrera

import android.app.Application

class DatosEst: Application() {
    var Nombre:String = ""
    var Codigo:Int = 0
    var Correo:String = ""

    override fun onCreate() {
        super.onCreate()
        val pref = getSharedPreferences("pref", MODE_PRIVATE)
        Nombre = pref.getString("nombres_defecto", "NN").toString()
        Codigo = pref.getInt("codigo_defecto", 1)
        Correo = pref.getString("correo_defecto", "nn@ucatolica.edu.co").toString()
    }
}