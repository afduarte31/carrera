package com.example.ucarrera

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MateriaViewHolder(val view: View): RecyclerView.ViewHolder(view) {

    //Recolectando las variables declaradas en el item_materia.xml

    //Demás datos de la materia
    val materiaCurso = view.findViewById<TextView>(R.id.tVCurso)
    val materiaSemestre = view.findViewById<TextView>(R.id.tVSemestre)
    val materiaProfesor = view.findViewById<TextView>(R.id.tVProfesor)

    //Imagen
    val materiaPic = view.findViewById<ImageView>(R.id.IVClase)

    fun show(materiaModel: Materia){
        //Anexandolos en sus respectivos items
        materiaCurso.text = materiaModel.Curso
        materiaSemestre.text = materiaModel.Semetre.toString()
        materiaProfesor.text = materiaModel.Profesor
        materiaModel.Image?.let { materiaPic.setImageResource(it) }
    }
}