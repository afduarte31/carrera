package com.example.ucarrera

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Materias : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_materias)
        setSupportActionBar(findViewById(R.id.toolbarPP))

        //Boton
        val btnResults:Button = findViewById(R.id.btnResultados)
        btnResults.setOnClickListener {
            startActivity(Intent(this, Resultados::class.java))
        }

        //Donde los datos estan almacenados
        val datos = application as DatosEst

        //Donde se imprimen los datos almacenados en la clase DatosEst.kt
        val tvShowNombre:TextView = findViewById(R.id.TVShowNombre)
        val tvShowCodigo:TextView = findViewById(R.id.TVShowCodigo)
        val tvShowCorreo:TextView = findViewById(R.id.TVShowCorreo)

        tvShowNombre.text = "Nombres: ${datos.Nombre}"
        tvShowCodigo.text = "Codigo: ${datos.Codigo}"
        tvShowCorreo.text = "Correo: ${datos.Correo}"

        initRecycler()
    }

    fun initRecycler(){
        val recycler:RecyclerView = findViewById(R.id.recycler_materias)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = MateriaAdapter(ListaMaterias.materiaList)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.Desarrolador->{
            startActivity(Intent(this, Desarrollador::class.java))
            true
        }
        R.id.Inicio->{
            startActivity(Intent(this, MainActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}