package com.example.ucarrera

import android.media.Image
import androidx.annotation.DrawableRes

//Variables para contener el RecyclerView
data class Materia(
    val Curso: String,
    val Semetre: Int,
    val Profesor: String,
    @DrawableRes
    val Image: Int?
)