package com.example.ucarrera

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbarPP))

        //Guardar los datos
        val datos = application as DatosEst

        //Llamando a los EditText del activity_main.xml
        val edNombre:EditText = findViewById(R.id.EDNombre)
        val edCodigo:EditText = findViewById(R.id.EDCodigo)
        val edCorreo:EditText = findViewById(R.id.EDCorreo)

        //Llamando a los Buttons del activity_main.xml
        val btnSig:Button = findViewById(R.id.btnSiguiente)
        btnSig.setOnClickListener {
            if(edNombre.text.isNotEmpty() && edCodigo.text.isNotEmpty() && edCorreo.text.isNotEmpty()){
                datos.Nombre = edNombre.text.toString()
                datos.Codigo = (edCodigo.text.toString()).toInt()
                datos.Correo = edCorreo.text.toString()
                startActivity(Intent(this, Materias::class.java))
            } else {
                Toast.makeText(this, "Faltan uno o mas datos por ingresar.", Toast.LENGTH_SHORT).show()
            }
        }
        val btnHD:Button = findViewById(R.id.btnHabeus)
        btnHD.setOnClickListener {
            startActivity(Intent(this, Habeas::class.java))
        }
        val btnBorr:Button = findViewById(R.id.btnBorrar)
        btnBorr.setOnClickListener {
            edNombre.setText("")
            edCodigo.setText("")
            edCorreo.setText("")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.Desarrolador->{
            startActivity(Intent(this, Desarrollador::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}