package com.example.ucarrera

class ListaMaterias {
    companion object{
        val materiaList = listOf<Materia>(
            Materia(
                Image = R.drawable.basededatos,
                Curso = "Bases de datos",
                Semetre = 4,
                Profesor= "Juanito Peréz"
            ),
            Materia(
                Image = R.drawable.estructuradedatos,
                Curso = "Estructura de datos",
                Semetre = 3,
                Profesor = "Benito Gúzman"
            ),
        )
    }
}